# Node v12
FROM node:12.18.3

# Create app directory
WORKDIR /usr/src/app

# For npm@5 or later, copy package-lock.json as well
COPY ./app/package.json ./
# COPY ./app/package.json package-lock.json ./

RUN npm install

RUN npm install --save-dev @11ty/eleventy

# Bundle app source
COPY ./app .

EXPOSE 8100
EXPOSE 35729
